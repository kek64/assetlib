
#include "CryptoDelegate.hpp"
#include "CommandLineParser.hpp"
#include <iostream>
#include <iterator>

#define UNUSED_PARAMETER(P) (P)

#if defined(_DEBUG)
#include <crtdbg.h>
#endif

std::unique_ptr<CommandLineParser> parser = nullptr;
std::unique_ptr<AssetManager> manager = nullptr;
std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

void keyAction();
void compressAction();
void appendAction();
void decompressAction();
void removeAction();
void infoAction();

void test()
{
	const auto sourceVec = std::vector<std::string> { 
		"E:/Py/Test/source/ee.txt",
		"E:/Py/Test/source/a.txt",
		"E:/Py/Test/source/Chrysanthemum.jpg"
	};

	const auto out = "E:/Py/Test/bin.bin";

	//manager->Compress(sourceVec, out);
	manager->DecompressToMemory(out);
	manager->RegisterHandler(std::make_shared< TextDataHandler >());

	if (const auto data = manager->FetchAsset<std::string>("a.txt"))
	{
		auto str = *data;
		std::replace(std::begin(str), std::end(str), 'k', 'w');
		std::cout << str << "\n";
	}

	std::system("pause");
}

int main(int argc, char ** argv)
{
	UNUSED_PARAMETER(argc);
	UNUSED_PARAMETER(argv);

#if defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_LEAK_CHECK_DF | _CRTDBG_ALLOC_MEM_DF);
	_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);
#endif

	std::cout << " *** AssetLib v.0.1 *** \n";

	parser = std::make_unique<CommandLineParser>(argc, argv);
	manager = CryptoDelegate::SharedManager();

	test();

	keyAction();
	compressAction();
	appendAction();
	decompressAction();
	removeAction();
	infoAction();

    return 0;
}

void compressAction()
{
	if (parser->CommandExists("-c"))
	{
		if (!parser->CommandExists("-o"))
			return;

		const auto filename = parser->CommandOption("-c");
		const auto output = parser->CommandOption("-o");
		manager->Compress(filename, output);
		std::cout << "Done!\n";
	}

	if (parser->CommandExists("-cc"))
	{
		if (!parser->CommandExists("-o"))
			return;

		const auto params = parser->CommandOption("-cc");
		const auto output = parser->CommandOption("-o");
		if (!params.empty())
		{
			const auto input = parser->Split(params, ',');
			std::copy(std::begin(input), std::end(input), std::ostream_iterator<std::string>(std::cout, "\n"));
			manager->Compress(input, output);
			std::cout << "Done!\n";
		}
	}
}

void appendAction()
{
	if (parser->CommandExists("-a"))
	{
		if (!parser->CommandExists("-o"))
			return;

		const auto filename = parser->CommandOption("-a");
		const auto output = parser->CommandOption("-o");
		manager->Append(filename, output);
		std::cout << "Done!\n";
	}

	if (parser->CommandExists("-aa"))
	{
		if (!parser->CommandExists("-o"))
			return;

		const auto params = parser->CommandOption("-aa");
		const auto output = parser->CommandOption("-o");
		if (!params.empty())
		{
			const auto input = parser->Split(params, ',');
			std::copy(std::begin(input), std::end(input), std::ostream_iterator<std::string>(std::cout, "\n"));
			manager->Append(input, output);
			std::cout << "Done!\n";
		}
	}
}

void decompressAction()
{
	if (parser->CommandExists("-d"))
	{
		if (!parser->CommandExists("-o"))
			return;

		const auto filename = parser->CommandOption("-d");
		const auto output = parser->CommandOption("-o");
		manager->DecompressToPath(filename, output);
		std::cout << "Done!\n";
	}

	if (parser->CommandExists("-dd"))
	{
		if (!parser->CommandExists("-o") || !parser->CommandExists("-b"))
			return;

		const auto params = parser->CommandOption("-dd");
		const auto binary = parser->CommandOption("-b");
		const auto output = parser->CommandOption("-o");
		if (!params.empty())
		{
			const auto input = parser->Split(params, ',');
			std::copy(std::begin(input), std::end(input), std::ostream_iterator<std::string>(std::cout, "\n"));
			manager->DecompressToPath(input, binary, output);
			std::cout << "Done!\n";
		}
	}
}

void removeAction()
{
	if (parser->CommandExists("-r"))
	{
		if (!parser->CommandExists("-o"))
			return;

		const auto filename = parser->CommandOption("-r");
		const auto output = parser->CommandOption("-o");
		manager->Remove(filename, output);
		std::cout << "Done!\n";
	}

	if (parser->CommandExists("-rr"))
	{
		if (!parser->CommandExists("-o"))
			return;

		const auto params = parser->CommandOption("-rr");
		const auto output = parser->CommandOption("-o");
		if (!params.empty())
		{
			const auto input = parser->Split(params, ',');
			std::copy(std::begin(input), std::end(input), std::ostream_iterator<std::string>(std::cout, "\n"));
			manager->Remove(input, output);
			std::cout << "Done!\n";
		}
	}
}

void infoAction()
{
	if (parser->CommandExists("-h"))
	{
		std::cout
			<< " -c C:/MyFolder/ -o C:/out.bin :: Create asset from folder path\n"
			<< " -cc filename1.txt,filename2.jpg -o C:/out.bin :: Create asset from files list\n"
			<< " -a C:/MyFolder/ -o C:/out.bin :: Append files to existed asset from folder\n"
			<< " -aa filename1.txt,filename2.jpg :: Append files to existed asset from files list\n"
			<< " -d C:/out.bin -o C:/MyFolder/ :: Decompress to path\n"
			<< " -dd filename1.txt,filename3.txt -b C:/out.bin -o C:/MyFolder/ :: Decompress to path\n"
			<< " -r filename.txt -o C:/out.bin :: Remove file from existed asset\n"
			<< " -rr filename1.txt,filename2.txt -o C:/out.bin :: Remove files from existed asset\n"
			<< " -f C:/out.bin :: Show files list from exited asset\n"
			<< " -k KEY :: Set custom key\n" 
			<< " -h :: Display command list\n\n";
	}

	if (parser->CommandExists("-f"))
	{
		const auto filename = parser->CommandOption("-f");
		const auto convertedFileName = fs::path(filename).filename().replace_extension("");

		manager->DecompressToMemory(filename);

		if (const auto asset = manager->FindAsset(converter.to_bytes(convertedFileName)))
		{
			for (const auto file : asset->fileNames)
				std::cout << file << "\n";
		}
	}
}

void keyAction()
{
	if (parser->CommandExists("-k"))
	{
		const auto key = parser->CommandOption("-k");
		if (!key.empty())
		{
			manager->SetKey(key);
		}
	}
}