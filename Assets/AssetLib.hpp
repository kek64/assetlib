
#pragma once

#ifndef _ASSET_LIB_HPP_
#define _ASSET_LIB_HPP_

#include <vector>
#include <unordered_map>
#include <memory>
#include <string>
#include <algorithm>
#include <functional>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

#ifdef _MSC_VER
#define AL_INLINE __forceinline
#elif __GNUC__
#define AL_INLINE __attribute__((always_inline))
#else
#define AL_INLINE inline
#endif


/*
	Binary file format
	|---------------------------|
	| FILES COUNT				|
	|---------------------------|
	| FILE NAME SIZE			|
	| FILE SIZE					|
	| FILE NAME				    |
	| .........					|
	|---------------------------|
	| FILE CHUNKS				|
	|---------------------------|
*/


/*!
	\brief Asset class.
	
	Contain loaded files and data from single binary file.
 */
struct Asset final
{
	std::vector< std::string > fileNames = {}; ///< list of asset file names
	std::vector< char* > ppFiles = {}; ///< list of raw data pointers to asset entry
	std::vector< size_t > uFileSizes = {}; ///< list of asset file sizes
	std::vector< size_t > uFileNameSizes = {}; ///< list of asset file name sizes
	std::string name = ""; ///< assset file name
	size_t totalFilesCount = 0; ///< total file count
	size_t size = 0; ///< asset total size
};


/*!
	\brief AssetHandler class.

	Used to convert asset data from/to raw bytes to custom data types.
*/
class AssetHandler
{
public:
	virtual ~AssetHandler() = default;
	template<typename T>
	std::shared_ptr<T> Transform(char *) { return std::shared_ptr<T>(); };
	virtual std::string Extension() = 0;
};


/*!
	\brief AssetManagerCryptoDelegate class.
	
	Used to encrypt/decrypt files entry and header file sizes.
 */
class AssetManagerCryptoDelegate
{
public:
	/*!
		Delegate Dtor.
	*/
	virtual ~AssetManagerCryptoDelegate() = default;

	/*!
		Set new cryptographic key.
		\param[in] key new cryptographic key string.
	*/
	virtual AL_INLINE void SetKey(const std::string &key) = 0;

	/*!
		Encrypt file size value.
		\param[inout] size file size.
	*/
	virtual AL_INLINE void EncryptSize(size_t &size) = 0;

	/*!
		Decrypt file size value.
		\param[inout] size file size.
	*/
	virtual AL_INLINE void DecryptSize(size_t &size) = 0;

	/*!
		Encrypt file data.
		\param[inout] data raw file data.
		\param[in] len raw file data length.
	*/
	virtual AL_INLINE void Encrypt(char *&data, size_t len) = 0;

	/*!
		Decrypt file data.
		\param[inout] data raw file data.
		\param[in] len raw file data length.
	*/
	virtual AL_INLINE void Decrypt(char *&data, size_t len) = 0;
};


/*!
	\brief AssetManager class.

	Used to manager custom binary format. Compress, update, decompress assets.
	Decompress assets into memory or folder.
 */
class AssetManager final
{
	using converter_t = std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>;
	using assets_t = std::vector< std::shared_ptr< Asset > >;
	using handlers_t = std::unordered_map< std::string , std::shared_ptr< AssetHandler > >;
	using delegate_t = std::unique_ptr<AssetManagerCryptoDelegate>;

	converter_t _converter;
	assets_t _assets;
	handlers_t _handlers;
	delegate_t _delegate;

	char filenameBuffer[256];

public:
	/*!
		Class Ctor
		\param[in] delegate cryptographics delegate class.
	 */
	explicit AssetManager(delegate_t &&delegate)
		: _delegate(std::move(delegate))
	{ }


	/*!
		Delete copy constructor.
	*/
	AssetManager(const AssetManager &) = delete;

	/*!
		Set default move constructor.
	*/
	AssetManager(AssetManager &&) = default;


	/*!
		Class Dtor. Unload loaded assets from memory.
	*/
	~AssetManager()
	{
		for (const auto asset : _assets)
			Unload(asset->name);
	
		_assets.clear();
	}


	/*!
		Delete copy-assign operator.
		\return lhs.
	*/
	AssetManager &operator=(const AssetManager &) = delete;

	/*!
		Set default move-assign operator.
		\return lhs.
	*/
	AssetManager &operator=(AssetManager &&) = default;


	/*!
		Set cipher key.
		\param[in] key cipher key.
	*/
	void SetKey(const std::string &key) const
	{
		_delegate->SetKey(key);
	}


	/*!
		Register asset handler.
		\param[in] handler pointer to custom data handler.
	*/
	void RegisterHandler(const std::shared_ptr< AssetHandler > &handler)
	{
		if (handler != nullptr && handler != std::shared_ptr<AssetHandler>())
		{
			const auto extension = handler->Extension();
			_handlers.insert({ extension, handler });
		}
	}


	/*!
		Remove asset handler.
		\param[in] extension asset handler data extension.
	*/
	void RemoveHandler(const std::string &extension)
	{
		_handlers.erase(extension);
	}


	/*!
		Compress files from folder into output binary file. Binary file will be automatically created.
		\param[in] folderPath root folder with files need compress into binary archive
		\param[in] outFilename output binary asset file name
	*/
	void Compress(const std::string &folderPath, const std::string &outFilename)
	{
		const auto pathes = ValidateFiles(folderPath);

		if (pathes.empty() || fs::is_directory(outFilename))
		{
			return;
		}

		Compress(pathes, outFilename);
	}


	/*!
		Compress files from vector into output binary file. Binary file will be automatically created.
		\param[in] filePathes list of file pathes need compress into binary archive
		\param[in] outFilename output binary asset file name
	*/
	void Compress(const std::vector< std::string > &filePathes, const std::string &outFilename)
	{
		const auto pathes = ValidateFiles(filePathes);

		if (pathes.empty() || fs::is_directory(outFilename))
		{
			return;
		}

		if (const auto asset = std::make_shared<Asset>())
		{
			std::string filename = "";
			std::string inFilePath = "";
			size_t fileNameSize = 0;
			size_t totalSize = 0;
			FILE *inFile = NULL;

			for (const auto &filePath : pathes)
			{
				const auto path = fs::path(filePath);
				filename = _converter.to_bytes(path.filename());
				fileNameSize = strlen(filename.c_str());
				inFilePath = _converter.to_bytes(path);

				fopen_s(&inFile, inFilePath.c_str(), "rb");
				if (inFile == NULL)
				{
					continue;
				}

				totalSize = FileSize(inFile);
				asset->totalFilesCount++;
				asset->size += totalSize;
				asset->uFileSizes.push_back(totalSize);
				asset->uFileNameSizes.push_back(fileNameSize);
				asset->fileNames.push_back(filename);

				const auto dataBuffer = new char[totalSize];
				memset(dataBuffer, 0, totalSize);
				fread(dataBuffer, sizeof(char), totalSize, inFile);
				asset->ppFiles.push_back(dataBuffer);

				fclose(inFile);
			}

			CompressAsset(outFilename, asset);
		}
	}


	/*!
		Load binary file into memory.
		NOTE: store loaded asset into _assets vector. For unload look at Unload method.
		\param[in] filename input binary asset file name.
	*/
	void DecompressToMemory(const std::string &filename)
	{
		if (!fs::is_directory(filename))
		{
			if (const auto asset = DecompressAsset(filename))
			{
				_assets.push_back(asset);
			}
		}
	}


	/*!
		Load binary file entry and save it into selected folder.
		\param[in] filename input binary asset file name.
		\param[in] outputPath output folder path.
	*/
	void DecompressToPath(const std::string &filename, const std::string &outputPath)
	{
		if (!fs::is_directory(outputPath) || fs::is_directory(filename))
		{
			return;
		}
		
		if (const auto asset = DecompressAsset(filename))
		{
			FILE *outFile = NULL;

			for (size_t i = 0; i < asset->totalFilesCount; ++i)
			{
				const auto out = outputPath + asset->fileNames[i];
				const auto dataPtr = asset->ppFiles[i];
				size_t totalSize = asset->uFileSizes[i];

				fopen_s(&outFile, out.c_str(), "wb");

				if (outFile != NULL)
				{
					fwrite(dataPtr, sizeof(char), totalSize, outFile);
					fclose(outFile);
				}
			}

			UnloadAsset(asset);
		}
	}

	/*!
		Load binary file entry and save selected files into selected folder.
		\param[in] files files to extract.
		\param[in] filename input binary asset file name.
		\param[in] outputPath output folder path.
	*/
	void DecompressToPath(const std::vector< std::string > files, const std::string &filename, const std::string &outputPath)
	{
		if (!fs::is_directory(outputPath) || fs::is_directory(filename))
		{
			return;
		}

		if (const auto asset = DecompressAsset(filename))
		{
			FILE *outFile = NULL;

			for (size_t i = 0; i < asset->totalFilesCount; ++i)
			{
				const auto out = outputPath + asset->fileNames[i];
				const auto dataPtr = asset->ppFiles[i];
				size_t totalSize = asset->uFileSizes[i];

				if (std::find(std::begin(files), std::end(files), asset->fileNames[i]) != std::end(files))
				{
					fopen_s(&outFile, out.c_str(), "wb");
					if (outFile != NULL)
					{
						fwrite(dataPtr, sizeof(char), totalSize, outFile);
						fclose(outFile);
					}
				}
			}

			UnloadAsset(asset);
		}
	}


	/*!
		Append files from folder into existed binary asset. Create asset file if not exists.
		\param[in] folderPath folder path contains files to be appended into asset.
		\param[in] outFile output binary asset file path.
	*/
	void Append(const std::string &folderPath, const std::string &outFile)
	{
		const auto pathes = ValidateFiles(folderPath);

		if (pathes.empty() || !fs::is_directory(folderPath) ||
			!fs::exists(outFile) || fs::is_directory(outFile))
		{
			return;
		}

		Append(pathes, outFile);
	}


	/*!
		Append files from file vector into existed binary asset. Create asset file if not exists.
		\param[in] filePathes list of file oathes to be appended into asset.
		\param[in] outFile output binary asset file path.
	*/
	void Append(const std::vector< std::string > &filePathes, const std::string &outFile)
	{
		const auto filteredPathes = ValidateFiles(filePathes);

		if (filteredPathes.empty())
		{
			return;
		}

		if (const auto asset = DecompressAsset(outFile))
		{
			std::string filename = "";
			std::string inFilePath = "";
			size_t fileNameSize = 0;
			size_t totalSize = 0;
			FILE *inFile = NULL;

			for (const auto &filePath : filteredPathes)
			{
				const auto path = fs::path(filePath);
				filename = _converter.to_bytes(path.filename());
				fileNameSize = strlen(filename.c_str());
				inFilePath = _converter.to_bytes(path);

				fopen_s(&inFile, inFilePath.c_str(), "rb");

				if (inFile == NULL)
				{
					continue;
				}

				totalSize = FileSize(inFile);
				asset->totalFilesCount++;
				asset->size += totalSize;
				asset->uFileSizes.push_back(totalSize);
				asset->uFileNameSizes.push_back(fileNameSize);
				asset->fileNames.push_back(filename);

				const auto dataBuffer = new char[totalSize];
				memset(dataBuffer, 0, totalSize);
				fread(dataBuffer, sizeof(char), totalSize, inFile);
				asset->ppFiles.push_back(dataBuffer);

				fclose(inFile);
			}

			CompressAsset(outFile, asset);
		}
	}


	/*!
		Remove file from existed binary file.
		\param[in] fileName name of file to be removed.
		\param[in] outFile target binary asset file path.
	*/
	void Remove(const std::string &fileName, const std::string &outFile)
	{
		Remove(std::vector<std::string>{ fileName }, outFile);
	}


	/*!
		Remove files from existed binary file.
		\param[in] fileNames list of file pathes to be removed from asset.
		\param[in] outFile target binary asset file path.
	*/
	void Remove(const std::vector< std::string > &fileNames, const std::string &outFile)
	{
		if (const auto asset = DecompressAsset(outFile))
		{
			size_t i = 0;
			size_t fileSize = 0;

			for (const auto removedFile : fileNames)
			{
				i = 0;
				for (const auto assetFile : asset->fileNames)
				{
					if (assetFile == removedFile)
					{
						fileSize += asset->uFileSizes[i];

						if (asset->ppFiles[i] != nullptr)
						{
							delete[] asset->ppFiles[i];
						}

						asset->ppFiles[i] = nullptr;
						asset->fileNames.erase(std::begin(asset->fileNames) + i);
						asset->ppFiles.erase(std::begin(asset->ppFiles) + i);
						asset->uFileNameSizes.erase(std::begin(asset->uFileNameSizes) + i);
						asset->uFileSizes.erase(std::begin(asset->uFileSizes) + i);
						asset->size -= fileSize;
						asset->totalFilesCount -= 1;
					}
					i++;
				}
			}

			if (fileSize > 0)
			{
				CompressAsset(outFile, asset);
			}
		}
	}


	/*!
		Unload asset from memory using asset name.
		\param[in] name asset name.
	*/
	void Unload(const std::string &name)
	{
		std::remove_if(std::begin(_assets), std::end(_assets), [&name = name](const auto &asset) {
			if (asset->name == name)
			{
				for (size_t i = 0; i < asset->ppFiles.size(); ++i)
				{
					if (asset->ppFiles[i] != nullptr)
					{
						delete[] asset->ppFiles[i];
						asset->ppFiles[i] = nullptr;
					}
				}

				asset->fileNames.clear();
				asset->ppFiles.clear();
				asset->uFileNameSizes.clear();
				asset->uFileSizes.clear();
				asset->totalFilesCount = 0;

				return true;
			}
			return false;
		});
	}


	/*!
		Unload asset from memory using asset pointer.
		\param[in] asset asset pointer.
	*/
	void Unload(const std::shared_ptr<Asset> &asset)
	{
		if (asset != nullptr || asset != std::shared_ptr<Asset>())
		{
			Unload(asset->name);
		}
	}
	

	/*!
		Find asset using asset name.
		\param[in] name asset name.
		\return finded asset or empty shared pointer if not found.
	*/
	std::shared_ptr< Asset > FindAsset(const std::string &name)
	{
		const auto finded = std::find_if(std::begin(_assets), std::end(_assets), [&name = name](const auto &asset) {
			return asset->name == name;
		});

		return (finded != std::end(_assets)) ? *finded : std::shared_ptr< Asset >();
	}


	/*!
		Find specific file in all loaded asset.
		\param[in] filename name of searched file with extension (image.jpg).
		\return finded raw data or nullptr if file not found.
	*/
	char* FindAssetData(const std::string &filename)
	{
		for (const auto &asset : _assets)
		{
			for (size_t i = 0; i < asset->fileNames.size(); ++i)
			{
				if (asset->fileNames[i] == filename)
				{
					return asset->ppFiles[i];
				}
			}
		}
		return nullptr;
	}

	/*!
			
	 */
	template<typename T>
	std::shared_ptr<T> FetchAsset(const std::string &filename)
	{
		const auto path = fs::path(filename);
		if (!path.has_extension())
		{
			return nullptr;
		}

		try
		{
			const std::string &extension = _converter.to_bytes(path.extension());
			if (const auto &handler = _handlers.at(extension))
			{
				if (const auto rawData = FindAssetData(filename))
				{
					const auto p = handler->Transform<T>(rawData);
					return p;
				}
			}

			return nullptr;
		}
		catch (...)
		{
			return nullptr;
		}
	}

private:

	/*!
		Compress asset to binary file using preloaded asset class instance.
		\param[in] filename target binary file path.
		\param[in] asset preloaded asset class pointer.
	*/
	AL_INLINE void CompressAsset(const std::string &filename, const std::shared_ptr< Asset > &asset)
	{
		if (asset != nullptr && asset != std::shared_ptr< Asset >())
		{
			FILE *outFile = NULL;

			fopen_s(&outFile, filename.c_str(), "wb");

			if (outFile == NULL)
			{
				return;
			}

			size_t totalFilesCount = asset->totalFilesCount;
			_delegate->EncryptSize(totalFilesCount);
			fwrite(&totalFilesCount, sizeof(size_t), 1, outFile);

			size_t filenameSize = 0;
			size_t fileSize = 0;
			size_t originalFilenameSize = 0;

			for (size_t i = 0; i < asset->totalFilesCount; i++)
			{
				filenameSize = asset->uFileNameSizes[i];
				originalFilenameSize = filenameSize;
				fileSize = asset->uFileSizes[i];
				const char* name = asset->fileNames[i].c_str();

				_delegate->EncryptSize(filenameSize);
				_delegate->EncryptSize(fileSize);

				fwrite(&filenameSize, sizeof(size_t), 1, outFile);
				fwrite(&fileSize, sizeof(size_t), 1, outFile);
				fwrite(name, sizeof(char), originalFilenameSize, outFile);
			}

			for (size_t i = 0; i < asset->ppFiles.size(); ++i)
			{
				fileSize = asset->uFileSizes[i];
				char* ptr = asset->ppFiles[i];
				_delegate->Encrypt(ptr, fileSize);
				fwrite(ptr, sizeof(char), fileSize, outFile);
			}

			UnloadAsset(asset);
			fclose(outFile);
		}
	}


	/*!
		Decompress asset from binary file.
		\param[in] filename target binary file path.
		\return pointer to loaded asset class.
	*/
	AL_INLINE std::shared_ptr< Asset > DecompressAsset(const std::string &filename)
	{
		size_t totalFilesCount = 0;
		FILE *inFile = NULL;

		if (!fs::exists(filename) || fs::is_directory(filename))
		{
			return std::shared_ptr< Asset >();
		}
		
		fopen_s(&inFile, filename.c_str(), "rb");

		if (inFile == NULL)
		{
			return std::shared_ptr< Asset >();
		}

		size_t binaryFileSize = FileSize(inFile);
		const auto pAsset = std::make_shared< Asset >();
		const auto fileName = fs::path(filename).filename().replace_extension("");
		pAsset->name = _converter.to_bytes(fileName);

		fread(&totalFilesCount, sizeof(size_t), 1, inFile);
		_delegate->DecryptSize(totalFilesCount);

		for (size_t i = 0; i < totalFilesCount; ++i)
		{
			memset(filenameBuffer, 0, 256);

			size_t fileSize = 0;
			size_t fileNameSize = 0;

			fread(&fileNameSize, sizeof(size_t), 1, inFile);
			fread(&fileSize, sizeof(size_t), 1, inFile);

			_delegate->DecryptSize(fileNameSize);
			_delegate->DecryptSize(fileSize);

			fread(filenameBuffer, sizeof(char), fileNameSize, inFile);

			pAsset->uFileSizes.push_back(fileSize);
			pAsset->uFileNameSizes.push_back(fileNameSize);
			pAsset->fileNames.push_back(filenameBuffer);
		}

		pAsset->totalFilesCount = totalFilesCount;
		pAsset->size = binaryFileSize;

		for (size_t i = 0; i < totalFilesCount; ++i)
		{
			size_t totalSize = pAsset->uFileSizes[i];

			char* dataPtr = new char[totalSize];
			memset(dataPtr, 0, totalSize);

			fread(dataPtr, sizeof(char), totalSize, inFile);
			_delegate->Decrypt(dataPtr, totalSize);
			pAsset->ppFiles.push_back(dataPtr);
		}

		fclose(inFile);
		return pAsset;
	}


	/*!
		Unload asset from memory using asset pointer (asset not loaded into manager _assets list).
		\param[in] asset pointer to preloaded asset class
	*/
	AL_INLINE void UnloadAsset(const std::shared_ptr<Asset> &asset)
	{
		if (asset != nullptr || asset != std::shared_ptr<Asset>())
		{
			for (size_t i = 0; i < asset->ppFiles.size(); ++i)
			{
				if (asset->ppFiles[i] != nullptr)
				{
					delete[] asset->ppFiles[i];
					asset->ppFiles[i] = nullptr;
				}
			}

			asset->fileNames.clear();
			asset->ppFiles.clear();
			asset->uFileNameSizes.clear();
			asset->uFileSizes.clear();
			asset->totalFilesCount = 0;
		}
	}

	
	/*!
		Validate file pathes vector.
		\param[in] filePathes list of file pathes.
		\return list contains valid file pathes.
	*/
	AL_INLINE std::vector< std::string > ValidateFiles(const std::vector< std::string > &filePathes)
	{
		std::vector< std::string > validatedPathes;

		std::copy_if(std::begin(filePathes), std::end(filePathes), std::back_inserter(validatedPathes), [](const auto &file) {
			return fs::exists(file) && !fs::is_directory(file);
		});

		return validatedPathes;
	}


	/*!
		Validate file pathes from folder path.
		\param[in] folderPath folder path.
		\return list contains valid file pathes.
	*/
	AL_INLINE std::vector< std::string > ValidateFiles(const std::string &folderPath)
	{
		std::vector< std::string > validatedPathes;

		if (!fs::is_directory(folderPath))
		{
			return {};
		}

		for (auto &filePath : fs::directory_iterator(folderPath))
		{
			if (!fs::is_directory(filePath.status()))
			{
				auto convertedFilePath = _converter.to_bytes(filePath.path().c_str());
				validatedPathes.push_back(convertedFilePath);
			}
		}

		return validatedPathes;
	}

	/*!
		Get file size.
		\param[in] fp opened file pointer.
		\return file size.
	*/
	AL_INLINE size_t FileSize(FILE *fp)
	{
		size_t fileSize = 0;
		fseek(fp, 0L, SEEK_END);
		fileSize = ftell(fp);
		fseek(fp, 0L, SEEK_SET);
		return fileSize;
	}

};

#endif //_ASSET_LIB_HPP_