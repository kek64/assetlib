
#pragma once

#ifndef _CRYPTO_DELEGATE_HPP_
#define _CRYPTO_DELEGATE_HPP_

#include "AssetLib.hpp"
#include <memory>

class CryptoDelegate
	: public AssetManagerCryptoDelegate
{
	std::string _key;
	size_t _keyLen;
	size_t _sole;
public:
	static std::unique_ptr< AssetManager > SharedManager()
	{
		auto manager = [] {
			const std::string key = {
				0xD, 0xE, 0xA, 0xD, 0xC, 0x0, 0xD, 0xE,
				0xD, 0xA, 0xD, 0xD, 0xA, 0xC, 0xE, 0xF,
				0xF, 0xE, 0xA, 0xD, 0xC, 0x0, 0x4, 0x5,
				0x0, 0x9, 0xD, 0x1, 0x3, 0x7, 0x6, 0x3,
			};
			return std::make_unique< AssetManager >(
				std::make_unique< CryptoDelegate >(key)
			);
		}();
		return std::move(manager);
	}
public:
	CryptoDelegate(const std::string &key)
		: _key(key), _keyLen(key.length()), _sole(static_cast<size_t>(_key[0]))
	{ }

	void SetKey(const std::string &key)
	{
		_key = key;
		_keyLen = key.length();
		_sole = static_cast<size_t>(_key[0]);
	}

	void Encrypt(char *&block, size_t len) override
	{
		for (size_t i = 0; i < len; ++i)
			block[i] = block[i] ^ _key[i % _keyLen] + static_cast<char>(i);
	}

	void Decrypt(char *&block, size_t len) override
	{
		for (size_t i = 0; i < len; ++i)
			block[i] = block[i] ^ _key[i % _keyLen] + static_cast<char>(i);
	}

	void EncryptSize(size_t &fileSize) override
	{
		fileSize += _sole;
	}

	void DecryptSize(size_t &fileSize) override
	{
		fileSize -= _sole;
	}
};

class TextDataHandler
	: public AssetHandler
{
public:
	~TextDataHandler()
	{ }

	template< typename T = std::string >
	std::shared_ptr<T> Transform(char *pData)
	{
		auto d = std::make_shared<T>(pData);
		return d;
	}

	std::string Extension() override
	{
		return ".txt";
	}
};

#endif //_CRYPTO_DELEGATE_HPP_