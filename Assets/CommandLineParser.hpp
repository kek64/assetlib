
#pragma once

#ifndef _COMMAND_LINE_PARSER_HPP_
#define _COMMAND_LINE_PARSER_HPP_

#include <vector>
#include <string>
#include <sstream>
#include <iostream>

class CommandLineParser
{
	std::vector< std::string > _tokens;
public:
	CommandLineParser(int &argc, char **argv)
	{
		for (int i = 0; i < argc; ++i)
			_tokens.push_back(std::string(argv[i]));
	}

	const std::string & CommandOption(const std::string &option) const
	{
		static const std::string emptyString = "";
		auto itr = std::find(std::begin(_tokens), std::end(_tokens), option);
		if (itr != std::end(_tokens) && ++itr != std::end(_tokens))
			return *itr;
		return emptyString;
	}

	bool CommandExists(const std::string &option)
	{
		return std::find(std::begin(_tokens), std::end(_tokens), option) != std::end(_tokens);
	}

	std::vector<std::string> Tokens() const
	{
		return _tokens;
	}

	std::vector< std::string > Split(const std::string &value, char delimiter)
	{
		std::vector< std::string > tokens;
		std::string token;
		std::istringstream tokenStream(value);

		while (std::getline(tokenStream, token, delimiter))
			tokens.push_back(token);
		
		return tokens;
	}
};

#endif //_COMMAND_LINE_PARSER_HPP_